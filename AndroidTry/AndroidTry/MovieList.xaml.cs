﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AndroidTry.Controller;
using AndroidTry.Models;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndroidTry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MovieList : ContentPage
    {
        private int page = 1;
        private List<MovieItem> movlist;
        private int lastPost = 0;
        private int total_page;
        private int total_list = 0;
        private int genre;

        public MovieList(int gen = 0)
        {
            InitializeComponent();
            genre = gen;
        }

        protected override void OnAppearing()
        {
            movlist = GetMovieList();
            ShowMovie.ItemsSource = movlist;
        }

        private void ShowMovie_RemainingItemsThresholdReached(object sender, EventArgs e)
        {
            if (page <= total_page)
            {
                page += 1;

                List<MovieItem> tempmov = new List<MovieItem>();
                tempmov = GetMovieList();

                movlist.AddRange(tempmov);

                ShowMovie.ItemsSource = null;
                ShowMovie.ItemsSource = movlist;

                lastPost = movlist.Count();
            }
            else
            {
                ShowMovie.ItemsSource = movlist;
            }
        }

        private List<MovieItem> GetMovieList()
        {
            GenrePageRepository genrepo = new GenrePageRepository();
            var gen = genrepo.GetGenre();

            MovieListPageRepository moviepro = new MovieListPageRepository();
            var res = moviepro.getMovie(genre.ToString(), page);

            var jlist = JsonConvert.DeserializeObject<GenreRepository>(gen);
            var mlist = JsonConvert.DeserializeObject<MovieRepository>(res);

            List<MovieItem> tmpmov = mlist.results;
            total_page = mlist.total_pages;
            List<GenresItem> genlist = jlist.genres;

            foreach (var i in tmpmov)
            {
                List<string> gname = new List<string>();
                foreach (int j in i.genre_ids)
                {
                    var a = genlist.Find(k => k.id == j);
                    gname.Add(a.name);
                }
                i.genre_name = string.Join(", ", gname);
                i.image_path = "https://image.tmdb.org/t/p/w500" + i.poster_path;
                i.id_set = i.id.ToString();
            }

            total_list += tmpmov.Count();

            return tmpmov;
        }

        private void TapGestureRecognizer_Tapped(object sender, SelectionChangedEventArgs e)
        {
            var args = e.ToString();
            Navigation.PushAsync(new MovieDetail(args));
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            string data = ((Button)sender).CommandParameter as string;

            Navigation.PushAsync(new MovieDetail(data));
        }
    }
}