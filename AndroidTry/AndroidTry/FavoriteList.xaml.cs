﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AndroidTry.Models;
using AndroidTry.Controller;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndroidTry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavoriteList : ContentPage
    {
        public FavoriteList()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            getFavoriteList();
        }

        private async void getFavoriteList()
        {
            try
            {
                List<string> genre = new List<string>();
                MovieDetailPageRepository detail = new MovieDetailPageRepository();
                List<DetailRepository> movlist = new List<DetailRepository>();

                base.OnAppearing();
                var getCollections = await App.MyDatabase.ReadMovie();

                if (getCollections.Count() > 0)
                {
                    foreach (var i in getCollections)
                    {
                        var dlist = JsonConvert.DeserializeObject<DetailRepository>(detail.GetDetail(i.id));
                        dlist.id_meta = i.id;
                        dlist.poster_path = "https://image.tmdb.org/t/p/w500/" + dlist.poster_path;

                        foreach (var j in dlist.genres)
                        {
                            genre.Add(j.name);
                        }
                        dlist.genre_name = string.Join(", ", genre);

                        movlist.Add(dlist);
                    }
                }
                else
                {
                    await DisplayAlert("Notification", "Favorite list is empty!", "OK");
                }

                MovieFavorite.ItemsSource = movlist;
            }
            catch (Exception e)
            {
                string errmsg = "Error : " + Convert.ToString(e.Message);
                await DisplayAlert("Error", errmsg, "OK");
            }
        }

        private void BtnDetails_Clicked(object sender, EventArgs e)
        {
            string data = ((Button)sender).CommandParameter as string;

            Navigation.PushAsync(new MovieDetailFav(data));
        }

        private void BtnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {
                string data = ((Button)sender).CommandParameter as string;
                App.MyDatabase.DeleteMovie(new MovieDBItem
                {
                    id = data
                });

                DisplayAlert("Success", "Movie has deleted from favorite list!", "OK");

                getFavoriteList();
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
                DisplayAlert("Error", "Error : " + errmsg, "OK");
            }
        }
    }
}