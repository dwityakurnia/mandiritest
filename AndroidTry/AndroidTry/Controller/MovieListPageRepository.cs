﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AndroidTry.Models;

namespace AndroidTry.Controller
{
    class MovieListPageRepository
    {
        private string movies;

        public string getMovie(string genre, int page)
        {
            getMovieList(genre, page);
            return movies;
        }

        private async void getMovieList(string genre, int page = 1)
        {
            string key = new AuthKey().GetKey();
            string BaseUrl = new AuthKey().getBaseURL();

            BaseUrl += "discover/movie?include_adult=false&include_video=true&language=en-US&page=" + page.ToString();
            BaseUrl += "&sort_by=popularity.desc";
            if(genre != "0")
            {
                BaseUrl += "&with_genres=" + genre;
            }

            using (var client = new HttpClient())
            {
                List<GenresItem> movie = new List<GenresItem>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                // send a GET request  
                Task<string> result = client.GetStringAsync(BaseUrl);
                result.Wait();
                movies = result.Result;
            }
        }
    }
}
