﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;
using AndroidTry.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AndroidTry.Controller
{
    class ReviewPageRepository
    {
        private string reviewlist;

        public string GetReview(string id, string page)
        {
            getReviewMovie(id, page);

            return reviewlist;
        }

        private async void getReviewMovie(string id, string page)
        {
            string key = new AuthKey().GetKey();
            string BaseUrl = new AuthKey().getBaseURL();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                // send a GET request  
                Task<string> result = client.GetStringAsync(BaseUrl + "movie/" + id + "/reviews?language=en-US&page=" + page);
                result.Wait();
                reviewlist = result.Result;
            }
        }
    }
}
