﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AndroidTry.Models;

namespace AndroidTry.Controller
{
    class MovieDetailPageRepository
    {
        private string moviedetail;
        private string ytlink;

        public string GetDetail(string id)
        {
            getDetailMovie(id);

            return moviedetail;
        }

        public string GetYTLink(string id)
        {
            getDetailYT(id);

            return ytlink;
        }

        private async void getDetailMovie(string id)
        {
            string key = new AuthKey().GetKey();
            string BaseUrl = new AuthKey().getBaseURL();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                // send a GET request  
                Task<string> result = client.GetStringAsync(BaseUrl + "movie/" + id);
                result.Wait();
                moviedetail = result.Result;
            }
        }

        private async void getDetailYT(string id)
        {
            string key = new AuthKey().GetKey();
            string BaseUrl = new AuthKey().getBaseURL();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                // send a GET request  
                Task<string> result = client.GetStringAsync(BaseUrl + "movie/" + id + "/videos");
                result.Wait();
                ytlink = result.Result;
            }
        }
    }
}
