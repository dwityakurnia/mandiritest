﻿using System;
using Microsoft.CSharp;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Threading.Tasks;
using AndroidTry.Models;
using System.Linq;
using Xamarin.Forms;

namespace AndroidTry.Controller
{
    class GenrePageRepository
    {
        private string genrelist;

        public string GetGenre()
        {
            getGenreMovie();

            return genrelist;
        }

        private async void getGenreMovie()
        {
            string key = new AuthKey().GetKey();
            string BaseUrl = new AuthKey().getBaseURL();

            using (var client = new HttpClient())
            {
                List<GenresItem> genre = new List<GenresItem>();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                // send a GET request  
                Task<string> result = client.GetStringAsync(BaseUrl + "genre/movie/list?language=en");
                result.Wait();
                genrelist = result.Result;
            }
        }

    }
}
