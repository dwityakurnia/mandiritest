﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidTry.Models
{
    class ReviewRepository
    {
        public int id { get; set; }
        public string id_meta { get; set; }
        public int page { get; set; }
        public List<ReviewResult> results { get; set; }
        public int total_pages { get; set; }
        public int total_results { get; set; }
    }

    class ReviewResult
    {
        public string author { get; set; }
        public string content { get; set; }
        public AuthDetail author_details { get; set; }
        public string created_at { get; set; }
        public string id { get; set; }
        public string updated_at { get; set; }
        public string get_rating { get; set; }
        public string get_avatar { get; set; }
    }

    class AuthDetail
    {
        public string name { get; set; }
        public string username { get; set; }
        public string avatar_path { get; set; }
        public double? rating { get; set; }
    }
}
