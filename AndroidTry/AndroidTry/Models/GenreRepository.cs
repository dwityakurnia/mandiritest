﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidTry.Models
{
    public class GenreRepository
    {
        public List<GenresItem> genres { get; set; }
    }

    public class GenresItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string meta { get; set; }
    }
}
