﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidTry.Models
{
    class MovieYTRepository
    {
        public int id { get; set; }
        public List<YTLinkDetail> results { get; set; }
        public string ytlink { get; set; }
    }

    class YTLinkDetail
    {
        public string id { get; set; }
        public string name { get; set; }
        public string key { get; set; }
    }
}
