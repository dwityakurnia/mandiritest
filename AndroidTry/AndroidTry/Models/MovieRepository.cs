﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace AndroidTry.Models
{
    class MovieRepository
    {
        public int page { get; set; }
        public List<MovieItem> results { get; set; }
        public int total_pages { get; set; }
        public int total_results { get; set; }
    }

    public class MovieItem
    {
        public int id { get; set; }
        public string id_set { get; set; }
        public string title { get; set; }
        public List<int> genre_ids { get; set; }
        public string genre_name { get; set; }
        public string overview { get; set; }
        public string original_language { get; set; }
        public string backdrop_path { get; set; }
        public string poster_path { get; set; }
        public string image_path { get; set; }
        public float vote_average { get; set; }
        public bool adult { get; set; }
        public string release_date { get; set; }
    }

    public class MovieDBItem
    {
        [PrimaryKey]
        public string id { get; set; }
    }
}
