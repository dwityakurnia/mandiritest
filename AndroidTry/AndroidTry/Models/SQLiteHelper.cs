﻿using System;
using System.Collections.Generic;
using SQLite;
using System.Text;
using System.Threading.Tasks;
using AndroidTry.Models;

namespace AndroidTry.Models
{
    public class SQLiteHelper
    {
        private readonly SQLiteAsyncConnection db;

        public SQLiteHelper(string dbPath)
        {
            db = new SQLiteAsyncConnection(dbPath);
            db.CreateTableAsync<MovieDBItem>();
        }

        public Task<int> CreateMovie(MovieDBItem movie)
        {
            return db.InsertAsync(movie);
        }

        public Task<List<MovieDBItem>> ReadMovie()
        {
            return db.Table<MovieDBItem>().ToListAsync();
        }

        public Task<int> UpdateMovie(MovieDBItem movie)
        {
            return db.UpdateAsync(movie);
        }

        public Task<int> DeleteMovie(MovieDBItem movie)
        {
            return db.DeleteAsync(movie);
        }
    }
}
