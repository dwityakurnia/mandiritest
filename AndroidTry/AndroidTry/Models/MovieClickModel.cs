﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace AndroidTry.Models
{
    class MovieClickModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public MovieClickModel()
        {
            getView = new Command(GoToDetail);
        }

        public ICommand getView { get; }

        void GoToDetail(object s)
        {
            string args = s.ToString();
        }
    }
}
