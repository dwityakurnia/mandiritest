﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidTry.Models
{
    class DetailRepository
    {
        public int id { get; set; }
        public string id_meta { get; set; }
        public string original_title { get; set; }
        public string imdb_id { get; set; }
        public int budget { get; set; }
        public bool adult { get; set; }
        public List<genlist> genres { get; set; }
        public string genre_name { get; set; }
        public string homepage { get; set; }
        public string original_language { get; set; }
        public string overview { get; set; }
        public string release_date { get; set; }
        public string poster_path { get; set; }
        public float vote_average { get; set; }
    }

    class genlist
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
