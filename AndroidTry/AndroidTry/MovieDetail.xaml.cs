﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using AndroidTry.Controller;
using AndroidTry.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndroidTry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MovieDetail : ContentPage
    {
        private string idmov;
        private int revpage = 1;
        private int tot_page = 0;
        private List<ReviewResult> listrev;

        public MovieDetail(string det = "")
        {
            InitializeComponent();
            idmov = det;
        }

        protected override void OnAppearing()
        {
            List<string> genre = new List<string>();
            MovieDetailPageRepository detail = new MovieDetailPageRepository();
            ReviewPageRepository review = new ReviewPageRepository();

            var vid = detail.GetYTLink(idmov);
            var det = detail.GetDetail(idmov);
            var rev = review.GetReview(idmov, revpage.ToString());

            var dlist = JsonConvert.DeserializeObject<DetailRepository>(det);
            var dvid = JsonConvert.DeserializeObject<MovieYTRepository>(vid);
            var rmov = JsonConvert.DeserializeObject<ReviewRepository>(rev);

            dlist.id_meta = dlist.id.ToString();

            if(dvid.results.Count() > 0)
            {
                dvid.ytlink = "https://www.youtube.com/embed/" + dvid.results[0].key;
            }

            LblTitle.Text = dlist.original_title;
            foreach(var i in dlist.genres)
            {
                genre.Add(i.name);
            }

            foreach(var i in rmov.results)
            {
                i.get_rating = Convert.ToString(i.author_details.rating);
                i.get_avatar = i.author_details.avatar_path;
            }

            ImgSrc.Source = "https://image.tmdb.org/t/p/w500/" + dlist.poster_path;
            LblGenre.Text = string.Join(", ", genre);
            LblRate.Text = Convert.ToString(dlist.vote_average) + "/10";
            LblDate.Text = "Release date : " + dlist.release_date;
            LblLang.Text = "Language : " + dlist.original_language;
            Lblimdb.Text = "IMDB : " + dlist.imdb_id;
            lblDesc.Text = dlist.overview;

            BtnFav.CommandParameter = dlist.id_meta;

            YTLinks.Source = dvid.ytlink;

            tot_page = rmov.total_pages;
            listrev = rmov.results;
            MovieReview.ItemsSource = listrev;
        }

        private void MovieReview_RemainingItemsThresholdReached(object sender, EventArgs e)
        {
            if(revpage < tot_page)
            {
                revpage += 1;

                ReviewPageRepository review = new ReviewPageRepository();
                var rev = review.GetReview(idmov, revpage.ToString());
                var rmov = JsonConvert.DeserializeObject<ReviewRepository>(rev);

                foreach (var i in rmov.results)
                {
                    i.get_rating = Convert.ToString(i.author_details.rating);
                    i.get_avatar = i.author_details.avatar_path;
                }

                listrev.AddRange(rmov.results);

                MovieReview.ItemsSource = null;
                MovieReview.ItemsSource = listrev;
            }
            else
            {
                MovieReview.ItemsSource = listrev;
            }
        }

        private void BtnFav_Clicked(object sender, EventArgs e)
        {
            try
            {
                string data = ((Button)sender).CommandParameter as string;
                App.MyDatabase.CreateMovie(new MovieDBItem
                {
                    id = data
                });

                DisplayAlert("Success", "Movie has added to favorite list!", "OK");
            }
            catch(Exception ex)
            {
                string errmsg = ex.Message;
                DisplayAlert("Error", "Error : " + errmsg, "OK");
            }
        }
    }
}