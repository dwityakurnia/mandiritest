﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AndroidTry.Controller;
using AndroidTry.Models;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndroidTry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GenrePage : ContentPage
    {
        private List<GenresItem> genlist;

        public GenrePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            GenrePageRepository genrepo = new GenrePageRepository();
            var res = genrepo.GetGenre();

            var jlist = JsonConvert.DeserializeObject<GenreRepository>(res);
            genlist = jlist.genres;
            foreach (var i in genlist)
            {
                i.meta = i.name.ToLower();
            }

            GenreList.ItemsSource = genlist;
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            string val = e.NewTextValue.ToLower();

            GenreList.ItemsSource = genlist.Where(s => s.meta.Contains(val));
        }

        private void GenreList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var Genre = e.Item as GenresItem;

            Navigation.PushAsync(new MovieList(Genre.id));
        }
    }
}