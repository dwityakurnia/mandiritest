﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using AndroidTry.Models;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace AndroidTry
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //NavigationPage.SetHasNavigationBar(this, false);

            List<MenuRepository> menu = new List<MenuRepository>
            {
                new MenuRepository{name="genre", text="Genre List"},
                new MenuRepository{name="movie", text="Movie List"},
                new MenuRepository{name="favorite", text="Favorite List"}
            };

            MainMenuList.ItemsSource = menu;
        }

        private void MainMenuList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var menuSel = e.Item as MenuRepository;
            ((ListView)sender).SelectedItem = null;
            //MainMenuList.SelectedItem = null;

            if (menuSel.name == "favorite")
            {
                Navigation.PushAsync(new FavoriteList());
            }
            else if (menuSel.name == "movie")
            {
                Navigation.PushAsync(new MovieList());
            }
            else
            {
                Navigation.PushAsync(new GenrePage());
            }
        }
    }
}
